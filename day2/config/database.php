<?php
//strat dbcon
function dbcon(){
$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=school", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  return $conn;
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
  die();
}

}//end dbcon

function get_student(){
    $conn=dbcon();
    $stmt = $conn->prepare("select s.id as ids,s.full_name,s.email,ls.matrix,ls.course,ls.id as idls
    FROM student s
    left join level_student  ls on ls.student_id =s.id");
    $stmt->execute();
  
    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
  return  $stmt->fetchAll();

    

}//end get_student


function get_student_byid($id){
    $conn=dbcon();
    $stmt = $conn->prepare("select s.id as ids,s.full_name,s.email,ls.matrix,ls.course,ls.id as idls
    FROM student s
    left join level_student  ls on ls.student_id =s.id
    where s.id=:id
    ");

    $stmt->bindParam(':id', $id);

    $stmt->execute();
  
    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
  return  $stmt->fetch();

    

}//end get_student_byid

function update_student_byid($id,$full_name,$email){
    $conn=dbcon();
    $stmt = $conn->prepare("UPDATE student
     SET full_name=:full_name,email=:email WHERE id=:id
    ");

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':full_name', $full_name);
    $stmt->bindParam(':email', $email);

    $stmt->execute();
  
    // set the resulting array to associative
   
  return  $stmt->rowCount();

    

}//end get_student_byid


function add_student($full_name,$email){
    $conn=dbcon();
    $stmt = $conn->prepare("INSERT INTO student (full_name, email)
    VALUES (:full_name, :email)");


    $stmt->bindParam(':full_name', $full_name);
    $stmt->bindParam(':email', $email);

    $stmt->execute();
    $last_id = $conn->lastInsertId();
    // set the resulting array to associative
   
  return  $last_id;

    

}//end get_student_byid


?>