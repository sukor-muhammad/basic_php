<html>
<body>

<?php
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    
    if (empty($_POST["name"])) {
      $nameErr = "Name is required";
    } else {
      $name = test_input($_POST["name"]);
    }

    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
      } else {
        $email = test_input($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          $emailErr = "Invalid email format";
        }
      }
}


// echo 'ini post <br>';
// echo '<pre>';
// print_r($_POST);
// echo '<pre>';

// echo 'ini get <br>';
// echo '<pre>';
// print_r($_GET);
// echo '<pre>';

// echo 'ini request <br>';
// echo '<pre>';
// print_r($_REQUEST);
// echo '<pre>';

?>

<form action="" method="post" >
Name: <input value='<?=$name??''?>' type="text" name="name"><br>
<span class="error">* <?php echo $nameErr??'';?></span>
<br><br>
E-mail: <input value='<?=$email??''?>' type="text" name="email"><br>
<span class="error">* <?php echo $emailErr??'';?></span>
<br><br>
<input type="submit">
</form>

</body>

